# TODO: SYSlog export

# https://getpractical.co.uk/2021/05/24/cisco-cucm-axl-api-requests-using-python/
# https://developer.cisco.com/docs/axl-schema-reference/
# https://developer.cisco.com/site/axl/
# https://github.com/bwks/axl/blob/master/foley.py
# https://collabapilab.ciscolive.com/lab/pod1/print

#start-up sandbox
# https://devnetsandbox.cisco.com/
# search Collab 14, set time or default is 6 hours which can be extended to 3 days

# create access group
# 1.  User management --> user settings --> access control group --> new add
# 2. name the group --> axl api access --> save
# 3. top right related links -->  assign role to ACP
# 4. find all --> select standard axl access --> add selected

# create axl user
# 1. User management --> application user --> add new
# 2. user id: axlaccess, password: axlpassword
# 3. add to access control group --> axl api access --> save

# user mgnt --> user add --> uni line template --> LN, FN
# user mgmt --> user settings --> user profile --> add --> name (lab), assign device and line TAG template + enable mobile
# user mgnt --> user add --> feature group template --> user profile change to lab
# device --> device setting -->  phone service --> add new EM service "Extension Mobility", http://10.10.20.1:8080/emapp/EMAppServlet?device=#DEVICENAME#

# system --> ldap --> sywstem --> change to telephone number
# rdp --> 10.10.20.100 --> tools menu --> add user Rick Flair, telephone 707081
# system --> ldap --> directory
# manager = abc\administrator
# ldap cn=users,dc=abc,dc=inc
# AGP --> standard CCM end users and standard CTI enabled
# feature group --> lab
# apply mask XXXXXX
# server --> 10.10.20.100

#find table name
# run sql select t.tabname, count(c.colname) as count from systables t, syscolumns c where t.tabid=c.tabid and c.colname like '% typerecordingflag%' group by t.tabname

# list UDP profile desk and user id
#run sql SELECT device.name,device.description,enduser.userid FROM device INNER JOIN enduser ON device.fkenduser=enduser.pkid AND device.tkClass=254

# list all users and attached end devices
# select enduser.userid, device.name from enduser,device,enduserdevicemap where enduserdevicemap.fkenduser=enduser.pkid and enduserdevicemap.fkdevice=device.pkid

#Devices and Device profiles (During EM) that have associate with enduser by userid filter
#select fkdevice from enduserdevicemap where fkenduser=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

#Device Mobility that have associate with enduser by userid filter
# select pkid from device where fkenduser_mobility=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

# All Devices that configured with Owner User ID field and have associate with enduser by userid filter
# select name from device where fkenduser=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

import urllib3
import zeep
import time
from pprint import pprint
import logging
from lxml import etree
from zeep.exceptions import Fault
from zeep.cache import SqliteCache
from zeep.transports import Transport
import requests
from requests import Session
from sys import exit
import random
from flask import Flask, render_template,request, make_response
import werkzeug
from werkzeug.utils import secure_filename
import csv
import io
import os
import ast
import xml.etree.ElementTree as ET
import re

CUCM_ADDRESS = '10.10.20.1'
CUCM_USERNAME = 'administrator'
CUCM_PASSWORD = 'ciscopsdt'

CUC_ADDRESS = '10.10.20.18'
CUC_USERNAME = 'administrator'
CUC_PASSWORD = 'ciscopsdt'

# Create variable for the CUCM 14 AXL WSDL File location
# from the services plugin axl toolkit download page
AXL_WSDL_FILE = "axlsqltoolkit/schema/current/AXLAPI.wsdl"

def show_history():
    for hist in [history.last_sent, history.last_received]:
        print(etree.tostring(hist["envelop"], encoding="unicode", pretty_print=true))

session = requests.Session()
session.verify = False
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
session.auth = requests.auth.HTTPBasicAuth(CUCM_USERNAME, CUCM_PASSWORD)
history = zeep.plugins.HistoryPlugin()
client = zeep.Client(wsdl=AXL_WSDL_FILE, transport=zeep.Transport(session=session), plugins=[history])
service = client.create_service('{http://www.cisco.com/AXLAPIService/}AXLAPIBinding',
                                f'https://{CUCM_ADDRESS}/axl/')
print(service.getCCMVersion())

def cuc_add_user(userid,telephone,type):
    print("Getting CUC info")

    # find LDAP user info

    url = f'http://{CUC_ADDRESS}/vmrest/import/users/ldap?query=(alias%20is%20{userid})'
    print(url)
    r = requests.get(url, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
    print(r.content)

    cuc_pkid = None
    root = ET.fromstring(r.content)
    for child in root.iter('*'):
        print(child.tag,child.text)

        if child.tag == "alias":
            cuc_alias = child.text
        if child.tag == "firstName":
            cuc_firstname = child.text
        if child.tag == "lastName":
            cuc_lastname = child.text
        if child.tag == "pkid":
            cuc_pkid = child.text

        #if child.tag == "ObjectId":
        #    object_id = child.text

    if cuc_pkid is not None:
        # Importing LDAP user
        headers = {'Content-type': 'application/json'}
        url = f'https://{CUC_ADDRESS}/vmrest/import/users/ldap?templateAlias=voicemailusertemplate'
        data = {
            "dtmfAccessId": telephone,
            "pkid":cuc_pkid
        }
        print(data)
        print (url)
        r = requests.post(url, json=data, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD),headers=headers)
        print(r.status_code,r.content)
        if r.status_code == 201:
            if type == "html":
                return("&#x2705; Successfully added VM dtmfAccessID:<b>"+str(telephone)+"</b>")
            else:
                return(",'added_vm':'True'")
        else:
            return("&#10062; Unable to add to CUC")
    else:
        return ("unable to import CUC user")


    return "Error import to CUC"

def find_user(search_type,query):
    print ("Searching for " + search_type + " = " +query)

    ####### User id #######
    if search_type == "userid":

        try:
            resp = service.listUser(searchCriteria={'userid': str(query)},
                                    returnedTags={'userid': '','firstName':'','lastName':'','status': '', 'telephoneNumber':'','mailid':''})
        except Fault:
            #show_history()
            print ("userid not found")

        try:
            #grab userid info

            user_list = resp['return'].user
            #print (resp['return'])
            for user in user_list:
                print("User Found")
                email_userid = user['mailid'][0:user['mailid'].index('@')]
                delete_info = "<a href=/remove_user?userid=" + email_userid + "&telephone="+user['telephoneNumber']+">&#10062;</a> "
                info = "userid: <b>" + user['userid'] +"</b>  <br>"
                info += "fullName: <b>" + user['firstName']+ " " + user['lastName'] +"</b><br>"
                info += "mailId: <b>" + user['mailid']+"</b><br>"

                #print(email_userid)
                name_info = "<br>firstName: <b>" + user['firstName']+"</b><br>lastName: <b>" + user['lastName']+"</b>"
                try:
                    info += "telephoneNumber: <b>" + user['telephoneNumber']+"</b><br>"
                except:
                    info += "telephoneNumber: <br>"
                #print (info)

            #search ldap state
            sql = "select userid, status, fkdirectorypluginconfig from enduser WHERE userid = '" + query +"'"
            resp = service.executeSQLQuery(sql)
            #print (resp['return']['row'])

            for row in resp['return']['row']:
                #print(row[2].text)
                if row[1].text == "1":
                    user_status = "Active"
                else:
                    user_status = "inActive"
                ldap_info = delete_info + "status: <b>" + user_status + "</b><br>"
                #print ("ldap state:"+str(row[2].text))
                if row[2].text is None:
                    ldap_info += "ldap_state: <b>Local User</b><br>"
                else:
                    ldap_info += "ldap_state: <b>LDAP User</b><br>"

            #print (name_info)
            return (ldap_info + info, "",True,name_info,email_userid)

        except:
            return ("&#x2705; Does not exist",None,False,"","none@none")

    ####### Telephonenumber #######
    elif search_type == "telephonenumber":
        print ("Searching for telephone number...")
        try:
            sql = "SELECT enduser.userid,enduser.firstName,enduser.lastName,enduser.telephoneNumber,enduser.mailid FROM enduser WHERE telephonenumber = " + query
            resp = service.executeSQLQuery(sql)
        except Fault:
            #show_history()
            print("Telephone not found")

        try:
            info = ""
            for row in resp['return']['row']:
                info += "<a href=remove_enduser_telephonenumber?userid=" + row[0].text + ">&#10062;</a> telephoneNumber: <b>" + row[3].text + "</b> <font color=red></font><br>"
                info += "userid: <b>" + row[0].text + "</b><br>"
                info += "fullName: <b>" + row[1].text + " " + row[2].text + "</b><br>"
                info +=  "mailId: <b>" +  row[4].text + "</b><br>"
                mailid = row[4].text[0:row[4].text.index('@')]

            return (info,row[0].text,True,mailid)
        except:
            return ("&#x2705; Not assigned",None,False,"none@none")

    ####### Directory #######
    elif search_type == "directorynumber":
        print ("Searching for directory number...")
        try:
            resp = service.listLine(searchCriteria={'pattern': query},
                                    returnedTags={'pattern': '','routePartitionName':'','asciiAlertingName':''})
        except Fault:
            show_history()

        #print(resp['return'])
        if resp['return'] is not None:
            line_list = resp['return'].line
            for  line in line_list:
                info = " <a href=/remove_line?uuid=" + line['uuid'] + ">&#10062;</a> pattern: <b>" + line['pattern'] + "</b> <br>"
                info += "routePartitionName: <b>" + str(line['routePartitionName']._value_1) + "</b><br>"
                info += "asciiAlertingName: <b>" + str(line['asciiAlertingName']) + "</b><br>"
            return (info,"",True,"none@none")
        else:
            return("&#x2705; Not Found ","",False,"none@none")

    ####### jabber phone #######
    elif search_type == "jabberProfile":
        try:
            resp = service.listPhone({'name': "CSF"+query}, returnedTags={'name': '','protocol': ''})
            #print (resp)
            if resp['return'] is not None:
                print("found jabber phone")
                return(" <div id=jabber_" + query + "><a href=/remove_phone?name=CSF"+query+">&#10062;</a> profileName:<br><b>"+resp['return']['phone'][0]['name'],"",True,"none@none")
        except Fault:
            show_history()
        return ("<div id=jabber_" + query + ">&#x2705; None","", False,"none@none")

    ####### device profile #######
    elif search_type == "UDP":
        try:
            resp = service.listDeviceProfile({'name': "UDP"+query}, returnedTags={'name': '','product': '','protocol': '','phoneTemplateName': ''})
            #print (resp)
            if resp['return'] is not None:
                return ("<div id=udp_"+query+"> <a href=/remove_udp?name=UDP" + query + ">&#10062;</a> profileName:<br><b>" + resp['return']['deviceProfile'][0]['name'] , "", True,"none@none")
        except Fault:
            show_history()
        return("<div id=udp_"+query+"> &#x2705; None","",False,"none@none")


app = Flask(__name__)
app.config['FILE_UPLOADS'] = "uploads"

@app.route('/rest/delete')
def rest_delete():
    userid = request.args.get("userid")
    if userid is None:
        return ("{'status':'False','msg':'userid is required'}")

    telephone = request.args.get("telephone")
    if telephone is None:
        return ("{'status':'False','msg':'telephone is required'}")

    status = remove_user_all_config(userid,telephone,"rest")
    return(status)

@app.route('/rest/add')
def rest_add():
    userid = request.args.get("userid")
    if userid == "":
        return ("{'add_user_status':'False','msg':'userid is required'}")

    telephone = request.args.get("telephone")
    if telephone == "":
        return ("{'add_user_status':'False','msg':'telephone is required'}")

    if userid == "" or telephone == "":
        return ("{'add_user_status':'False','msg':'userid and telephone are required'}")

    if request.args.get("add_user") == "true":
        add_user = True
    else:
        add_user = False

    clean = re.compile(r'<[^>]+>')
    rest_status = True
    rest_json = ",'userid':'"+userid+"'"
    rest_json += ",'telephone':'" + telephone + "'"

    user_id_match = True
    check_dn=True
    check_jabber=True
    check_udp=True
    check_vm=True

    status_ldap = full_ldap_sync()
    print (status_ldap)
    rest_json += ",'ldap_status':'"+status_ldap+"'"

    userid_output, tmp, found_userid, userinfo, email_userid = find_user("userid", userid)
    print("Found userid:" + str(found_userid))
    #print("test!!!!!!!!!!!")
    #print("-----------------",userid_output, tmp, found_userid, userinfo, email_userid)

    if found_userid is False:
        print("userid wasn't found, checking by telephone")
        userid_output, tmp, found_userid, userinfo, email_userid = find_user("userid", telephone)

    if email_userid != userid and found_userid == True:
        rest_json += ",'error_userid':'Userid's do not match'"
        user_id_match = False
        rest_status = False
        return ("{'add_user_status':'False','msg':'userid mismatch, userid:" + userid + " vs emailid:" + email_userid + " and telephone:"+telephone+"'}")

    userid_output = re.sub(clean, '', userid_output)
    print(userid_output)
    rest_json += ",'output_userid':'"+userid_output[9:]+"'"

    telephonenumber_output, tmp, found_telephonenumber, email_userid = find_user("telephonenumber", telephone)
    #print(email_userid)

    if email_userid != userid and found_telephonenumber == True:
        if  userid == found_telephonenumber:
            user_id_match = True
            rest_status = True
        else:
            rest_json += "'error_telephone':'Userid's do not match'"
            user_id_match = False
            rest_status = False

    print(telephonenumber_output)
    telephonenumber_output = re.sub(clean, '', telephonenumber_output)
    print(telephonenumber_output)
    rest_json += ",'output_telephone':'"+ str(telephonenumber_output[9:])+"'"

    if user_id_match == False:
        rest_json += ",'userid_match':'False'"
        rest_status = False
    else:
        rest_json += ",'userid_match':'True'"


    dn_output, tmp, found_dn, email_userid = find_user("directorynumber", telephone)
    print(str(found_dn))
    print(str(dn_output))
    if found_dn == True:
        rest_json += ",'found_dn':'True'"
    else:
        rest_json += ",'found_dn':'False'"
        rest_status = False
        check_dn = False

    jbr_output, tmp, found_jbr, email_userid = find_user("jabberProfile", telephone)
    #print(str(found_jbr))
    #print (str(jbr_output))
    if found_jbr == True:
        rest_json += ",'found_jabber':'True'"
        rest_status = False
        check_jabber =  False
    else:
        rest_json += ",'found_jabber':'False'"


    udp_output, tmp, found_udp, email_userid = find_user("UDP",telephone)
    #print(str(found_udp))
    #print(str(udp_output))
    if found_udp == True:
        rest_json += ",'found_udp':'True'"
        rest_status = False
        check_udp = False
    else:
        rest_json += ",'found_udp':'False'"

    # GET USER OBJECT_ID
    url = f'http://{CUC_ADDRESS}/vmrest/users?query=(alias%20is%20{userid})'
    #print (url)
    r = requests.get(url, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
    #print(r.content)
    root = ET.fromstring(r.content)

    found_alias = False
    for child in root.iter('*'):
        #print(child.tag,child.text)
        if child.tag == "Alias":
            found_alias = True
            rest_status = False
            rest_json += ",'found_vm':'True'"
            check_vm = False

    #print(str(found_alias))
    if found_alias == False:
        rest_json += ",'found_vm':'False'"

    if rest_status == True and add_user == True:
        if user_id_match == True and check_dn == True and check_jabber == True and check_udp == True and check_vm == True:
            print("Rest adding user")

        rest_json += add_upd(telephone,"rest")
        time.sleep(1)
        rest_json += add_jabberPhone(telephone,"rest")
        time.sleep(1)
        rest_json += cuc_add_user(userid,telephone,"rest")

    return("{'add_user_status':'"+str(rest_status)+"'"+rest_json+"}")


@app.route('/add_user')
def add_user():
    if len(request.args.get("userid")) == 0:
        return ("")
    else:
        userid = request.args.get("userid")

    #resp = service.listUser(searchCriteria={'userid': '707080'},returnedTags={})
    #print(resp)
    # https://community.cisco.com/t5/management/convert-ldap-user-to-local-user/td-p/3467514
    try:
        resp = service.addUser(user={
            'userid':userid,
            'ldapDirectoryName':'LAB',
            'lastName':'Flair',
            'presenceGroupName':'Standard Presence group',
            'userIdentity':'rflair@abc.inc'
        })
        print(resp)
    except Fault:
        show_history()
        return ("Unable to add Userid:"+userid+" <br><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")

    return ("Userid:"+userid+" has been added<br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",1000);</script><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")

def remove_user_all_config(userid,telephone,type):
    html = ""
    json_return = ""
    try:
        print("deleting user:",userid,telephone,type)
        userid_type = userid
        try:
            html += "deleting userid: " + userid + "<br>"
            resp = service.updateUser(userid=userid, ldapDirectoryName="", userIdentity="")
            print(resp)

            html += "converted user to local to be removed<br>"
            resp = service.removeUser(userid=userid)

            html += "<b>deleted userid: " + userid + "</b><BR>"
            json_return += ",'deleted_userid':'True'"
            userid_type = userid
        except:
            print ("Unable to delete by userid, trying my telephone")
            userid_type = telephone

        #print(userid_type,userid)
        if userid_type != userid:
            try:
                html += "deleting userid: " + telephone + "<br>"
                resp = service.updateUser(userid=telephone, ldapDirectoryName="", userIdentity="")
                #print(resp)
                html += "converted user to local to be removed<br>"
                resp = service.removeUser(userid=telephone)
                html += "<b>deleted userid: " + telephone + "</b><BR>"
                json_return += ",'deleted_userid':'True'"
            except:
                print ("unable to delete user by telephone")
                json_return += ",'error':'True'"

        print("deleting jabber phone")
        try:
            html += "deleting user jabber phone<br>"
            resp = service.removePhone(name="CSF" + userid_type)
            html += "<b>deleted phone:CSF" + userid_type + "</b><br>"
            json_return += ",'deleted_jabber':'True'"
        except:
            html += "unable to delete phone<br>"
            json_return += ",'deleted_jabber':'False'"

        print("deleting device profile")
        try:
            html += "deleting user device profile<br>"
            resp = service.removeDeviceProfile(name="UDP" + userid_type)
            html += "<b>deleted device profile:  UDP" + userid_type + "</b><br>"
            json_return += ",'deleted_udp':'True'"
        except:
            html += "unable to delete device profile<br>"
            json_return += ",'deleted_udp':'False'"

        print("deleting lines")
        html += "finding line / directory number: " + telephone + "<br>"
        resp = service.listLine(searchCriteria={'pattern': telephone},
                                returnedTags={'pattern': '', 'routePartitionName': '', 'asciiAlertingName': ''})
        #print(resp['return'])
        if resp['return'] is not None:
            line_list = resp['return'].line
            # print (line_list)
            for line in line_list:
                if line['pattern'] == telephone:
                    #print(line)
                    line_name = telephone
                    uuid = line['uuid']
        try:
            html += "removing user directory number:" + line_name + " / uuid" + uuid + "<br>"
            service.removeLine(uuid=uuid)
            html += "<b>removed line / directory number: " + line_name + "</b><br>"
            json_return += ",'deleted_dn':'True'"
        except:
            html += "unable to delete line<br>"
            json_return += ",'deleted_dn':'False'"

        try:
            html += "deleting user VM services <br>"
            # GET USER OBJECT_ID
            url = f'http://{CUC_ADDRESS}/vmrest/users?query=(alias%20is%20{userid})'
            print (url)
            r = requests.get(url, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
            print(r.content)
            root = ET.fromstring(r.content)

            alias_found = None
            for child in root.iter('*'):
                # print(child.tag,child.text)
                if child.tag == "Alias":
                    # print(child.tag, child.text)
                    alias_found = child.text
                if child.tag == "ObjectId":
                    objectid = child.text
                if child.tag == "DtmfAccessId":
                    DtmfAccessId = child.text

            # resp = service.removePhone(name=name)
            url = f'https://{CUC_ADDRESS}/vmrest/users/{objectid}'
            print(url)
            r = requests.delete(url, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
            print(r.status_code, r.content)
            html += "<b>successfully removed VM service</b><br>"
            json_return += ",'deleted_voicemail':'True'"
        except:
            html += "unable to delete voicemail<br>"
            json_return += ",'deleted_voicemail':'False'"

        if type == "html":
            return ("<font color=red><b>Userid:" + userid + " has been deleted</b></font> <br><br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",2500);</script><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button><br><br>" + html)
        else:
            return("{'userid:'"+userid+"','telephone':'"+telephone+"','user_deleted':'True'" + json_return+"}")
    except:
        if type == "html":
            return ("Unable to delete Userid:" + userid + ", something went wrong or it's been already deleted<br><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button><br><br>" + html)
        else:
            return("{'user_deleted':'False'"+json_return+"}")


@app.route('/remove_user')
def remove_user():
    if len(request.args.get("userid")) == 0:
        return ("")
    else:
        userid = request.args.get("userid")
        telephone = request.args.get("telephone")

    html = remove_user_all_config(userid,telephone,"html")
    return(html)


@app.route('/remove_line')
def remove_line():
    if len(request.args.get("uuid")) == 0:
        return ("")
    else:
     uuid = request.args.get("uuid")

    try:
        service.removeLine(uuid=uuid)
        html = """   <script type="text/javascript"> if(performance.navigation.type == 2){location.reload(true);}</script>"""
        return (html+"Directory number has been deleted<br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",1000);</script><button onclick=\"document.referrer ? window.location = document.referrer : document.referrer ? window.location = document.referrer : history.back();;\">Go Back</button>")
    except:
        return ("Unable to delete the Directory number, something went wrong or it's already been deleted<br><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")


@app.route('/remove_enduser_telephonenumber')
def remove_enduser_telephonenumber():
    if len(request.args.get("userid")) == 0:
        return ("")
    else:
     userid = request.args.get("userid")

    try:
        resp = service.updateUser(userid=userid,telephoneNumber='')
        return ("TelephoneNumber deleted from userid:"+userid+"<br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",1000);</script><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")
    except:
        return ("TelephoneNumber for an LDAP user cannot be deleted, please localize and delete the user<br><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")

@app.route('/remove_udp')
def remove_udp():
    if len(request.args.get("name")) == 0:
        return ("")
    else:
     name = request.args.get("name")

    try:
        resp = service.removeDeviceProfile(name=name)
        return (name+" user deviceProfile has been deleted<br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",1000);</script><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")
    except:
        return ("unable to delete "+ name+" user deviceProfile, either something wennt wrong or it's already been deleted<br><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")


@app.route('/remove_phone')
def remove_phone():
    if len(request.args.get("name")) == 0:
        return ("")
    else:
     name = request.args.get("name")

    try:
        resp = service.removePhone(name=name)
        return (name+" phone has been deleted<br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",1000);</script><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")
    except:
        return ("unable to delete" + name+" phone. something went wrong<br><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")

@app.route('/remove_vm')
def remove_vm():
    if len(request.args.get("objectid")) == 0:
        return ("")
    else:
     objectid = request.args.get("objectid")

    try:
        #resp = service.removePhone(name=name)
        url = f'https://{CUC_ADDRESS}/vmrest/users/{objectid}'
        print(url)
        r = requests.delete(url, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
        print(r.status_code, r.content)
        return ("User Voicemail box has been deleted<br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",1000);</script><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")
    except:
        return ("unable to delete VM. something went wrong<br><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>")


def add_upd(userid,type):
    html = "Creating UDP<br>"
    try:
        resp = service.addDeviceProfile(deviceProfile={
            'name': "UDP" + str(userid),
            'protocol': 'SIP',
            'product': 'Cisco 7965',
            'protocolSide': 'User',
            'phoneTemplateName': 'Standard 7965 SIP',
            'class': 'Device Profile',
            'lines':{
                'line':{
                    'index':1,
                    'dirn':{
                        'pattern':userid,
                        'routePartitionName': 'internal'
                    }
                }
            },
            'services': {'service': [{
                'telecasterServiceName': 'Extension Mobility',
                'name': 'Extension Mobility'
            }]}
        })
        html += "Successfully created <b>UDP" + str(userid)+"</b><br>"

    except:
        show_history()
        html += "<b><font color=red>Device phone profile already exists!</font></b><br>"
    # might need on services --? 'url': 'http://{0}:8080/emapp/EMAppServlet?device=#DEVICENAME#&EMCC=#EMCC#'.format(self.cucm)

    time.sleep(1)

    print("Associating Ext. Mob. to UDP<br>")
    html += "Associating Ext. Mob. to userid: " + userid + "<br>"
    try:
        device_profile = 'UDP' + str(userid)
        print(device_profile)
        resp = service.updateUser(
            userid=userid,
            enableMobility='true',
            phoneProfiles={'profileName': 'UDP' + userid}
        )
        html += " Success!"
        html = "&#x2705; " + html
        if type =="html":
            return (html)
        else:
            return(",'added_udp':'True'")
    except Fault:
        # show_history()
        return ("Userid does not exist, unable to associate device profile to the user")

@app.route('/add_user_device_profile')
def add_user_device_profile():
    if len(request.args.get("userid")) == 0:
        return ("")
    else:
     userid = request.args.get("userid")

    html = add_upd(userid,"html")
    return (html)

def add_jabberPhone(userid,type):
    html = "Creating Jabber Phone<br>"

    ldap_resp = service.listCss(searchCriteria='%',returnedTags={})
    print("get line uuid:")
    #print(ldap_resp)
    #for filter_name in ldap_resp['return']['ldapFilter']:
    #    print(filter_name['name'])


    try:
        print("Creating User Jabber Phone<br>")
        resp = service.addPhone(phone={
            'name': "CSF" + str(userid),
            'protocol': 'SIP',
            'product': 'Cisco Unified Client Services Framework',
            'protocolSide': 'User',
            'class': 'Phone',
            'devicePoolName':'Default',
            'ownerUserName': str(userid),
            'securityProfileName': 'Cisco Unified Client Services Framework - Standard SIP Non-Secure Profile',
            'sipProfileName': 'Standard SIP Profile',
            'phoneTemplateName': 'Standard Client Services Framework',
            'commonPhoneConfigName':'Standard Common Phone Profile',
            'locationName': 'Hub_None',
            'callingSearchSpaceName':{

            },
            'lines': {
                'line': {
                    'index': 1,
                    'dirn': {
                        'pattern': userid,
                        'routePartitionName': 'internal'
                    }
                }
            }
        })
        html += "Successfully added phone: <b>CSF" + str(userid) + "</b><br>"
    except Fault:
        #show_history()
        print("failed adding jabber phone")
        #return ("Userid does not exist, unable to create jabber phone associated to the userid")
        html += "<b><font color=red>Jabber Phone profile already exists!</font></b><br>"

    html += "Adding Jabber Phone to userid: " + userid + "<br>"
    print("adding jabber to ext mob")

    try:
        resp = service.updateUser(
            userid=userid,
            enableMobility='true',
            associatedDevices={'device': 'CSF'+userid},
        )
        #print (resp)
        print ("Successfully created: <b>CSF"+str(userid)+"</b><br>")
    except Fault:
        #show_history()
        return("Userid does not exist, unable to associate jabber phone to the user")

    html += "Jabber Phone successfully associated to userid"
    html = "&#x2705; " + html

    if type == "html":
        return (html)
    else:
        return (",'added_jabber':'True'")

@app.route('/add_jabber_phone')
def add_jabber_phone():
    if len(request.args.get("userid")) == 0:
        return ("")
    else:
        userid = request.args.get("userid")
    html = add_jabberPhone(userid,"html")
    return (html)

@app.route('/add_vm')
def add_vm():
    if len(request.args.get("userid")) == 0:
        return ("")
    else:
        userid = request.args.get("userid")
        telephone = request.args.get("telephone")
    html = cuc_add_user(userid,telephone,"html")
    return (html)


def full_ldap_sync():
    ldap_resp = service.doLdapSync(name='lab', sync='true')

    time.sleep(5)
    ldap_resp = service.getLdapSyncStatus(name='lab')
    print(ldap_resp)
    html = ldap_resp['return']
    return(html)

@app.route('/sync_ldap')
def sync_ldap():
    try:
        if request.args.get("full_sync") == "true":
            full_sync = True
        if full_sync:
            ldap_resp = service.doLdapSync(name='lab', sync='true')
            html = "Performing LDAP Sync, please wait<br>"
            time.sleep(5)
            ldap_resp = service.getLdapSyncStatus(name='lab')
            print(ldap_resp)
            html += ldap_resp['return'] + "<br>"
            html += "<br><br><script>setTimeout(\"document.referrer ? window.location = document.referrer : history.back();\",1000);</script><button onclick=\"document.referrer ? window.location = document.referrer : history.back();\">Go Back</button>"
            return (html)
    except:
        print ("Full LDAP sync variable not found")

@app.route('/user_search', methods=("POST", "GET"))
def user_search():

    cookie_user_list = request.cookies.get('user_list')
    user_list = []
    print("cookie_user_list",cookie_user_list)
    if cookie_user_list is not None:
        user_list = ast.literal_eval(cookie_user_list)
        print(user_list)
    else:
        if request.method == 'POST':
            print("File upload detected, creating userlist")
            uploaded_file = request.files['csv_file']  # This line uses the same variable and worked fine
            filepath = os.path.join(app.config['FILE_UPLOADS'], uploaded_file.filename)
            uploaded_file.save(filepath)
            with open(filepath) as file:
                csv_file = csv.reader(file)
                header = next(csv_file) # strip hearder
                for row in csv_file:
                    #print(row[0],row[1])
                    user_list.append({'user':row[0], 'telephone':row[1]})
        else:
            try:
                user_list.append({'user':request.args.get("userid"), 'telephone': request.args.get("telephonenumber")})
            except:
                user_list.append({'user': "", 'telephone': ""})


    html = """<title>CUCM User Import</title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    
    <script>
    function addUPD(userid) {
        document.getElementById("udp_"+userid).innerHTML = "Creating User deviceProfile";
        
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            document.getElementById("udp_"+userid).innerHTML = this.responseText;
        }
        
        xhttp.open("GET", "add_user_device_profile?userid="+userid);
        xhttp.send();
    }
    function addJabber(userid) {
        document.getElementById("jabber_"+userid).innerHTML = "Creating jabber phone";
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            document.getElementById("jabber_"+userid).innerHTML = this.responseText;
        }
        
        xhttp.open("GET", "add_jabber_phone?userid="+userid);
        xhttp.send();
    }
    function addVM(userid,telephone) {
        document.getElementById("VM_"+userid).innerHTML = "Creating VM Service";
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            document.getElementById("VM_"+userid).innerHTML = this.responseText;
        }
        
        xhttp.open("GET", "add_vm?userid="+userid+"&telephone="+telephone);
        xhttp.send();
    }

    </script>
    <style>
    table { 
        border-collapse: collapse; 
    }

    table thead td {
	    background-color: #D3D0C9 ;
        //color: #ffffff;
        font-weight: bold;
        font-size: 13px;
        border: 1px solid #54585d;
        width: 15%;
    } 
    table tbody td {
        color: #636363;
        border: 1px solid #dddfe1;
    }
    table tbody tr {
        background-color: #f9fafb;
    }
    table tbody tr:nth-child(odd) {
        background-color: #ffffff;
    }
    tbody th {
      background-color: #6D7483  ;
      color: #fff;
      text-align: right;
      //font-size: 15px;
      font-weight: normal;
      padding-right: 5;
      border-bottom: 1pt solid black;
    }
    
    tr.bottom_border td {  
        border-bottom: 1pt solid black; 
    }
    </style> 
    
    <center><font size=7>CUCM User Info Query</font></b> <br>[<a href=/>Back to Main Search</a>] <a href=sync_ldap?full_sync=true>[Initiate Full LDAP Sync]</a> [<a href="javascript:window.location.href=window.location.href">Refresh Page</a>]</center>
    <br>
    <table>
    <tr>
        <td align=middle valign=middle>
     <thead>
        <tr>
            <td>User Search Variables
            <td>Userid Match
            <td>endUser telephoneNumber Match
            <td>directoryNumber Match
            <td>jabberPhone Match
            <td>user deviceProfiles Match
            <td>VM Service Status
        </thead>
    <tbody>
    """
    print(user_list)
    for info in user_list:
        if info['user'] is None:
            return("Userid must not be blank<br><a href=/>Back</a>")

        html += "<tr class=bottom_border><th>userid: <b>" + info['user'] + "</b><br>telephonerNumber <b>" + info['telephone'] + "</b><br>directoryNumber: <b>" + info['telephone'] + "</b>"

        #print(info['user'])
        userid_output,tmp,found_userid,userinfo,email_userid = find_user("userid",info['user'])

        print ("Found userid:"+str(found_userid))
        #print(userid_output,tmp,found_userid,userinfo,email_userid)
        if found_userid is False:
            print("userid wasn't found, checking by telephone")
            userid_output, tmp, found_userid, userinfo, email_userid = find_user("userid", info['telephone'])
            #print(userid_output, tmp, found_userid, userinfo, email_userid)
            #html += "<br><b><font color=lightred>userid and telephone number userid do not match!<b></font>"
        #if len(userinfo) > 0:
        #    html += userinfo

        user_id_match = True
        if email_userid != info['user'] and found_userid == True:
            userid_output += "<center><font color=red><b>!!! Userid's do not match !!!"
            user_id_match = False
            bg_color = "#F3E8EA"
        else:
            bg_color = ""

        #else:
        html += "<td valign=top bgcolor="+bg_color+">"+str(userid_output)

        #if found_userid == False:
        #    html += "<br><a href=/sync_ldap?userid=" + info['user'] + "&telephone=" + info['telephone'] + ">&#x2795 Sync userid from LDAP</a>"

        telephonenumber_output,tmp,found_telephonenumber,email_userid = find_user("telephonenumber", info['telephone'])
        print (email_userid)
        if email_userid != info['user'] and found_telephonenumber == True:
            telephonenumber_output += "<center><font color=red><b>!!! Userid's do not match !!!"
            user_id_match = False
            bg_color = "#F3E8EA"
        else:
            bg_color = ""
        html += "<td  valign=top bgcolor="+bg_color+">"+str(telephonenumber_output)

        dn_output,tmp,found_dn, email_userid = find_user("directorynumber", info['telephone'])
        html += "<td  valign=top>"+str(dn_output)

        jbr_output,tmp,found_jbr, email_userid = find_user("jabberProfile", info['telephone'])
        html += "<td  valign=top>"+str(jbr_output)
        #print (found_jbr)
        if found_userid == True and found_jbr == False:
            html+= "<br><br><button onclick = 'addJabber(" + info['telephone'] + ")'> &#x2795 Add jabberPhone</button></div>"

        udp_output,tmp,found_udp, email_userid = find_user("UDP", info['telephone'])
        html += "<td  valign=top>"+str(udp_output)
        if found_userid == True and found_udp == False:
                html += "<br><br><button onclick='addUPD("+info['telephone']+")'>&#x2795 Add deviceProfile</button></div>"

        userid = info['user']
        html += "<td valign=top><div id=VM_"+userid +">"
        #cuc_output = cuc_add_user(info['user'])


        # GET USER OBJECT_ID
        url = f'http://{CUC_ADDRESS}/vmrest/users?query=(alias%20is%20{userid})'
        # print (url)
        r = requests.get(url, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
        # print(r.content)
        root = ET.fromstring(r.content)

        alias_found = None
        for child in root.iter('*'):
            #print(child.tag,child.text)
            if child.tag == "Alias":
                #print(child.tag, child.text)
                alias_found = child.text
            if child.tag == "ObjectId":
                objectid = child.text
            if child.tag == "DtmfAccessId":
                DtmfAccessId = child.text

        if found_userid == False and alias_found is None:
            html += ("&#x2705; None")
        elif found_userid == True and alias_found is None:
            if user_id_match == False:
                html += ("&#x2705; None")
            else:
                html += ("&#x2705; None<br><br><button onclick='addVM(\""+info['user']+"\",\""+info['telephone']+"\")'>&#x2795 Add VM Service</button>")
        else:
            html+= ("<a href=remove_vm?objectid="+objectid+">&#10062;</a> VM dtmfAccessID:<b>"+ DtmfAccessId)

    html += "</table><b><br><center>"
    resp = make_response(html)
    resp.set_cookie('user_list', str(user_list))
    return resp


@app.route('/')
def main_page():
    html = """
    <title>CUCM User Import</title>
    <script>
    function checkEmpty() {
        checkFields = true;
            var valueofId=document.getElementById("userid").value;
            if (!valueofId) {
                checkFields=false;
            }
            var valueofId=document.getElementById("telephonenumber").value;
            if (!valueofId) {
                checkFields=false;
            }
            var valueofId=document.getElementById("csv_file").value;
            if (valueofId) {
                valueofId=document.getElementById("userid").value = "";
                valueofId=document.getElementById("telephonenumber").value = ""
                document.getElementById("form").setAttribute("method", "POST");
                checkFields=true;
            }   
                
            if (checkFields == true) {
                document.cookie = "user_list=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                document.form.submit();
            } else {
                alert ("Please fill out both userid and telephoneNumber fields or select a csv file to search against");
                return;
            } 
    }
    </script>
    <center>
    <form name=form id=form method=get action=user_search enctype="multipart/form-data"> 
    <table border=0 width=50%>
        <tr><td colspan=2 align=center><b>Onboard New Employee to CUCM and CUC Services </b>
        <tr><td colspan=2  align=right><hr>
        <tr>
            <td align=right>userid:<td><input type=text name=userid id=userid>
        <Tr>
            <td align=right>telephoneNumber:<td><input type=text name=telephonenumber id=telephonenumber>

        <tr><td colspan=3><center><i> - or -
        <tr><td align=right> Upload CSV:<td> <input id=csv_file name=csv_file type="file" accept=".csv" /> 
        <tr><td align=right>Example CSV:<td><a href=example_import.csv>Example User CSV File</a>
        <tr><td>
        <tr><td colspan=6 align=center><input id=submit_form style="width:100%;height:30px;color:blue;" type=button value="Query CUCM user info" onclick="return checkEmpty()">
        <tr><td colspan=6><hr>
        <tr><td colspan=6> 
           <u><b>Web Interface Information</b> </u>
        <ol>
        <li>Searches CUCM for the user by userid, then telephone, returns if local or LDAP
        <ol style="list-style-type: disc;">
            <li> <i> Searches can be either individual or by csv upload, please download example template above for csv searches</i>
            <li> <i>Properly sync'd LDAP users should have the correct userid, telephone, and directory number assigned to them</i></il>
        </ol>
        <li>Displays found user info, directory number, jabber phone, user device profile, and voicemail
        <li>For new/clean users, administrators are able to add jabber phone, user device profile, and voicemail services
        <ol style="list-style-type: disc;">
            <li> <i>These associated services, along with services like extension mobility, will be correctly mapped to the specified user</i>
        </ol>
        <li>Administrators are able to delete users and services using the X button at the top left corner of each service
        </ol>
        <br>
        <u><b>Rest interface URL References</b></u>
        <ul>
        <li>Check user add status &#8594;   /rest/add?userid={{userid}}&telephone={{telephone}}
        <ul style="list-style-type: disc;">
            <li> returns json add_user_status True (success) or False (failed) with various pertinent user and service details
        </ul>
        <li>Add error free user using the add_user=true &#8594;  /rest/add?userid={{userid}}&telephone={{telephone}}&add_user=true
        <ul style="list-style-type: disc;">
            <li> <i>Only userid that are error free with no existing user device profiles, jabber, or VM will be added</i>
            <li><i> Newly added services will be displayed as 'found_vm':'False','added_udp':'True','added_jabber':'True','added_vm':'True'</i>
        </ul>
        <li>Delete user &#8594;  /rest/delete?userid={{userid}}&telephone={{telephone}}
        <ul>
            <li>  <i>*Only matched userid + emailuserid that are error free will be deleted, all associated services will be deleted as well i.e. userid, dn, upd, jabber, and vm</i>
        </ul>
        </table>
        </form>
        <script>
            var input = document.getElementById("telephonenumber");
            input.addEventListener("keypress", function(event) {
              if (event.key === "Enter") {
                event.preventDefault();
                document.getElementById("submit_form").click();
              }
            });
        </script>
    """

    return html


if __name__ == '__main__':
    app.run(debug=True)
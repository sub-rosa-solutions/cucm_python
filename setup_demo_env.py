
# todo: unity ldap

# https://getpractical.co.uk/2021/05/24/cisco-cucm-axl-api-requests-using-python/
# https://developer.cisco.com/docs/axl-schema-reference/
# https://developer.cisco.com/site/axl/
# https://github.com/bwks/axl/blob/master/foley.py
# https://collabapilab.ciscolive.com/lab/pod1/print

#start-up sandbox
# https://devnetsandbox.cisco.com/
# search CUCM 14, set time or default is 6 hours which can be extended to 3 days

# create access group
# 1.  User management --> user settings --> access control group --> new add
# 2. name the group --> axl api access --> save
# 3. top right related links -->  assign role to ACP
# 4. find all --> select standard axl access --> add selected

# create axl user
# 1. User management --> application user --> add new
# 2. user id: axlaccess, password: axlpassword
# 3. add to access control group --> axl api access --> save

# ALL THE BELOW IS AUTOMATED
# user mgnt --> user add --> uni line template --> LN, FN
# user mgmt --> user settings --> user profile --> add --> name (lab), assign device and line TAG template + enable mobile
# user mgnt --> user add --> feature group template --> user profile change to lab
# device --> device setting -->  phone service --> add new EM service "Extension Mobility", http://10.10.20.1:8080/emapp/EMAppServlet?device=#DEVICENAME#

# system --> ldap --> sywstem --> change to telephone number
# rdp --> 10.10.20.100 --> tools menu --> add user Rick Flair, telephone 707081
# system --> ldap --> directory
# manager = abc\administrator
# ldap cn=users,dc=abc,dc=inc
# AGP --> standard CCM end users and standard CTI enabled
# feature group --> lab
# apply mask XXXXXX
# server --> 10.10.20.100

#find table name
# run sql select t.tabname, count(c.colname) as count from systables t, syscolumns c where t.tabid=c.tabid and c.colname like '% typerecordingflag%' group by t.tabname

# list UDP profile desk and user id
#run sql SELECT device.name,device.description,enduser.userid FROM device INNER JOIN enduser ON device.fkenduser=enduser.pkid AND device.tkClass=254

# list all users and attached end devices
# select enduser.userid, device.name from enduser,device,enduserdevicemap where enduserdevicemap.fkenduser=enduser.pkid and enduserdevicemap.fkdevice=device.pkid

#Devices and Device profiles (During EM) that have associate with enduser by userid filter
#select fkdevice from enduserdevicemap where fkenduser=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

#Device Mobility that have associate with enduser by userid filter
# select pkid from device where fkenduser_mobility=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

# All Devices that configured with Owner User ID field and have associate with enduser by userid filter
# select name from device where fkenduser=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

import urllib3
import zeep
import time
from pprint import pprint
import logging
from lxml import etree
from zeep.exceptions import Fault
from zeep.cache import SqliteCache
from zeep.transports import Transport
import requests
from requests import Session
from sys import exit
import random
from flask import Flask, render_template,request
import werkzeug
from werkzeug.utils import secure_filename
import csv
import io
import os
import datetime
import ldap
import ldap.modlist as modlist
import grp
import math
import time
from os import getenv

CUCM_ADDRESS = '10.10.20.1'
CUCM_USERNAME = 'administrator'
CUCM_PASSWORD = 'ciscopsdt'

LDAP_HOST = '10.10.20.100'
LDAP_ADMIN_DN = 'abc\\administrator'
LDAP_PASS = 'ciscopsdt'
LDAP_BASE_DN = 'cn=Users,dc=abc,dc=inc'
HOME_BASE = "/home"

# Create variable for the CUCM 14 AXL WSDL File location
# from the services plugin axl toolkit download page
AXL_WSDL_FILE = "axlsqltoolkit/schema/current/AXLAPI.wsdl"

def show_history():
    for hist in [history.last_sent, history.last_received]:
        print(etree.tostring(hist["envelop"], encoding="unicode", pretty_print=true))

session = requests.Session()
session.verify = False
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
session.auth = requests.auth.HTTPBasicAuth(CUCM_USERNAME, CUCM_PASSWORD)
history = zeep.plugins.HistoryPlugin()
client = zeep.Client(wsdl=AXL_WSDL_FILE, transport=zeep.Transport(session=session), plugins=[history])
service = client.create_service('{http://www.cisco.com/AXLAPIService/}AXLAPIBinding',
                                f'https://{CUCM_ADDRESS}/axl/')
print(service.getCCMVersion())

#######################################
#       Create partitions
#######################################
# call routing --> class of control --> partition --> internal + external
print("Adding partition internal")
try:
    resp = service.addRoutePartition({'name': 'internal'})
    print(resp)
except Fault:
    print ("partition internal exists")
    #show_history()

print("Adding partition ext")
try:
    resp = service.addRoutePartition({'name': 'external'})
    print(resp)
except Fault:
    print ("partition ext exists")
    #show_history()

#######################################
#       Create CSS
#######################################
#call routing --> class of control --> call search space --> internal to internal, ext to ext, unrestricted ext + int
print("Adding CSS internal")
try:
    resp = service.addCss({
            'name': 'internal',
            'members': {
                'member': {
                    'routePartitionName': 'internal',
                    'index': '1',
                }
            }
         }
    )
    print(resp)
except Fault:
    print ("CSS internal exists")
    #show_history()

print("Adding CSS external,")
try:
    resp = service.addCss({
            'name': 'external',
            'members': {
                'member': {
                    'routePartitionName': 'external',
                    'index': '1',
                }
            }
        }
    )
    print(resp)
except Fault:
    print ("CSS external exists")
    #show_history()

print("Adding CSS unrestricted")
try:
    resp = service.addCss( {
            'name': 'unrestricted',
            'members': {
                'member': [
                    {'index':1, 'routePartitionName': 'external'},
                    {'index':2, 'routePartitionName': 'internal'}
                ]
            }
        }
    )

    print(resp)
except Fault:
    print ("CSS unrestricted exists")
    #show_history()

#######################################
#       Create universal line template
#######################################
# user mgnt --> user add --> uni line template --> LN, FN
print("Adding universal line template")
try:
    resp = service.addUniversalLineTemplate({
        'name': 'lab',
        'lineDescription': '#LN#,#FN#',
        'alertingName': '#NAME#',
        'blfPresenceGroup':'Standard Presence group',
        'partyEntranceTone':'Default',
        'autoAnswer':'',
        'voiceMailProfile':'DefaultVM',
        'routePartition':'internal',
        'callingSearchSpace':'internal',

        'noAnsIntCallsDestination':'Voicemail',
        'noAnsIntCallsCss':'internal',
        'noAnsExtCallsDestination':'Voicemail',
        'noAnsExtCallsCss':'internal',

        'unregisteredIntCallsDestination':'Voicemail',
        'unregisteredIntCallsCss':'internal',
        'unregisteredExtCallsDestination':'Voicemail',
        'unregisteredExtCallsCss':'internal',

        'fwdDestInternalCallsWhenNotRetrieved':'Voicemail',
        'cssFwdInternalCallsWhenNotRetrieved':'internal',

        'busyIntCallsDestination':'Voicemail',
        'busyIntCallsCss':'internal',
        'busyExtCallsDestination':'Voicemail',
        'busyExtCallsCss':'internal',

        'noCoverageIntCallsDestination':'Voicemail',
        'noCoverageIntCallsCss':'internal',
        'noCoverageExtCallsDestination':'Voicemail',
        'noCoverageExtCallsCss':'internal',

        'ctiFailureDestination':'Voicemail',
        'ctiFailureCss':'internal',

        'noAnswerRingDuration':'15'
    })
    print(resp)
except Fault:
    print ("ULT already exists")
    #show_history()

#######################################
#       Create user profile
#######################################
# user mgmt --> user settings --> user profile --> add --> name (lab), assign device and line TAG template + enable mobile
print("adding user profile")
try:
    resp = service.addUserProfileProvision({
        'name': 'lab',
        'deskPhones': 'Sample Device Template with TAG usage examples',
        'mobileDevices': 'Sample Device Template with TAG usage examples',
        'profile': 'Sample Device Template with TAG usage examples',
        'universalLineTemplate': 'lab'
    })
    print(resp)
except Fault:
    print ("User Profile already exists")
    #show_history()


#######################################
#       Create feeature group template
#######################################
# user mgnt --> user add --> feature group template --> user profile change to lab
print("Adding feature group template")
try:
    resp = service.addFeatureGroupTemplate({
        'name': 'lab',
        'serviceProfile': 'Default User Profile',
        'userProfile': 'lab',
        'enableMobility': 'true',
        'enableMobileVoiceAccess': 'false',
        'BLFPresenceGp': 'Standard Presence group',
        'maxDeskPickupWait':'10000',
        'subscribeCallingSearch': '',
        'remoteDestinationLimit':'4',
        'enableEMCC': 'false',
        'meetingInformation': 'false',

    })
    print(resp)
except Fault:
    print ("Feature group template already exists")
    #show_history()

#######################################
#       Create Ext Mobility service
#######################################
# device --> device setting -->  phone service --> add new EM service "Extension Mobility", http://10.10.20.1:8080/emapp/EMAppServlet?device=#DEVICENAME#
print("Adding ipPhoneService - Ex Mobility")
try:
    resp = service.addIpPhoneServices({
        'serviceName': 'Extension Mobility',
        'asciiServiceName': 'Extension Mobility',
        'serviceUrl': 'http://10.10.20.1:8080/emapp/EMAppServlet?device=#DEVICENAME#',
        'serviceCategory': 'XML Service',
        'serviceType': 'Standard IP Phone Service'
    })
    print(resp)
except Fault:
    print ("ipPhone Service already exists")
    #show_history()


#######################################
#       Toggle LDAP sync my telephone
#######################################
# system --> ldap --> sywstem --> change to telephone number
print("Updating LDAP system to telephonenumber")
try:
    resp = service.updateLdapSystem(userIdAttribute='telephoneNumber')
    print(resp)
except Fault:
    print ("LDAP system telephonenumber already updated")
    #show_history()

#######################################
#       Create LDAP directory
#######################################
# system --> ldap --> directory
# manager = abc\administrator
# ldap cn=users,dc=abc,dc=inc
# AGP --> standard CCM end users and standard CTI enabled
# feature group --> lab
# apply mask XXXXXX
# server --> 10.10.20.100
#https://github.com/jeokrohn/ucmprovisioning/blob/master/setup.py
print("Adding LDAP directory")
try:
    next_exec = datetime.datetime.now() + datetime.timedelta(days=1)
    next_exec = next_exec.isoformat()[:10]
    next_exec = '{} 00:00'.format(next_exec)
    print(next_exec)

    ldap_directory = {
        'name': 'lab',
        'ldapDn': 'administrator@abc.inc',
        'ldapPassword': 'ciscopsdt',
        'userSearchBase': 'cn=Users,dc=abc,dc=inc',
        'repeatable': 'true',
        'intervalValue': '6',
        'scheduleUnit': 'HOUR',
        'nextExecTime': next_exec,
        'middleName': 'middleName',
        'phoneNumber': 'telephoneNumber',
        'mailId': 'mail',
        'directoryUri': 'mail',
        'featureGroupTemplate': 'lab',
        'applyPoolList': 'false',
        'applyMask':'true',
        'mask':'XXXXXX',
        'servers': {
            'server': [
                {
                'hostName': '10.10.20.100',
                'ldapPortNumber': '389',
                "sslEnabled": 'false'
            }
          ]
        },
        'accessControlGroupInfo': {
            'accessControlGroupName': [
                {'accessControlGroup': 'Standard CCM End Users'},
                {'accessControlGroup': 'Standard CTI Enabled'}
            ]
        }
    }
    resp = service.addLdapDirectory(ldapDirectory=ldap_directory)
    print("Succesfully added LDAP Directory")
    print(resp)
except Fault:
    print ("error adding ldap directory")
    #show_history()


#######################################
#       Create LDAP test users
#######################################
# rdp --> 10.10.20.100 --> tools menu --> add user Rick Flair, telephone 707081
# Connect to LDAP
ldap_users_ou_dn = "cn=users,dc=abc,dc=inc"
conn = ldap.initialize('ldap://' + LDAP_HOST)
conn.protocol_version = 3
conn.set_option(ldap.OPT_REFERRALS, 0)
conn.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
conn.simple_bind_s(LDAP_ADMIN_DN, LDAP_PASS)

def create_user(user):
    userid = user['username']
    fullname = user['firstname'] + ' ' + user['lastname']
    telephoneNumber = user['telephone']
    email = user['username'] + "@abc.inc"
    enable_account = str("512") #514 is disable

    attrs = {
        'objectClass': [
            'top'.encode('utf-8'),
            'person'.encode('utf-8'),
            'organizationalPerson'.encode('utf-8'),
            'user'.encode('utf-8')
        ],
        'cn': fullname.encode("utf-8"),
        'userPrincipalName': email.encode("utf-8"),
        'displayName': fullname.encode("utf-8"),
        'givenName': user['firstname'].encode("utf-8"),
        'sn': user['lastname'].encode("utf-8"),
        'sAMAccountName': userid.encode("utf-8"),
        'mail': email.encode("utf-8"),
        'telephoneNumber': telephoneNumber.encode("utf-8"),
        'userAccountControl':enable_account.encode("utf-8")
    }

    # Set up user dn
    user_cn = fullname
    user_dn = "CN={},{}".format(user_cn, ldap_users_ou_dn)

    # Create user
    try:
        attrs_ldif = modlist.addModlist(attrs)
        conn.add_s(user_dn, attrs_ldif)
        print ("+Added userid:"+userid)
    except:
        print("LDAP userid:"+userid+" already exists")

#LOOP THROUGH CSV AND ADD USERS
users_to_import = []
print("Adding CSV users to LDAP")
with open('./demo_ldap_users.csv', 'r') as users_csv_file:
    users_reader = csv.reader(users_csv_file)
    for row in users_reader:
        if row:
            #print(row)
            user = {
                'username': row[0],
                'firstname': row[1],
                'lastname': row[2],
                'telephone':row[3]
            }
            if user['username'] is not None:
                create_user(user)

# SYNC LDAP
print("Performing LDAP Sync, please wait")
ldap_resp = service.doLdapSync(name='lab', sync='true')
time.sleep(3)
ldap_resp = service.getLdapSyncStatus(name='lab')
print(ldap_resp)

print ("Finished setting up CUCM 14 Devnet Lab Environment")
exit ()





# todo: unity ldap


# https://getpractical.co.uk/2021/05/24/cisco-cucm-axl-api-requests-using-python/
# https://developer.cisco.com/docs/axl-schema-reference/
# https://developer.cisco.com/site/axl/
# https://github.com/bwks/axl/blob/master/foley.py
# https://collabapilab.ciscolive.com/lab/pod1/print

#start-up sandbox
# https://devnetsandbox.cisco.com/
# search CUCM 14, set time or default is 6 hours which can be extended to 3 days

# create access group
# 1.  User management --> user settings --> access control group --> new add
# 2. name the group --> axl api access --> save
# 3. top right related links -->  assign role to ACP
# 4. find all --> select standard axl access --> add selected

# create axl user
# 1. User management --> application user --> add new
# 2. user id: axlaccess, password: axlpassword
# 3. add to access control group --> axl api access --> save

# ALL THE BELOW IS AUTOMATED
# user mgnt --> user add --> uni line template --> LN, FN
# user mgmt --> user settings --> user profile --> add --> name (lab), assign device and line TAG template + enable mobile
# user mgnt --> user add --> feature group template --> user profile change to lab
# device --> device setting -->  phone service --> add new EM service "Extension Mobility", http://10.10.20.1:8080/emapp/EMAppServlet?device=#DEVICENAME#

# system --> ldap --> sywstem --> change to telephone number
# rdp --> 10.10.20.100 --> tools menu --> add user Rick Flair, telephone 707081
# system --> ldap --> directory
# manager = abc\administrator
# ldap cn=users,dc=abc,dc=inc
# AGP --> standard CCM end users and standard CTI enabled
# feature group --> lab
# apply mask XXXXXX
# server --> 10.10.20.100

#find table name
# run sql select t.tabname, count(c.colname) as count from systables t, syscolumns c where t.tabid=c.tabid and c.colname like '% typerecordingflag%' group by t.tabname

# list UDP profile desk and user id
#run sql SELECT device.name,device.description,enduser.userid FROM device INNER JOIN enduser ON device.fkenduser=enduser.pkid AND device.tkClass=254

# list all users and attached end devices
# select enduser.userid, device.name from enduser,device,enduserdevicemap where enduserdevicemap.fkenduser=enduser.pkid and enduserdevicemap.fkdevice=device.pkid

#Devices and Device profiles (During EM) that have associate with enduser by userid filter
#select fkdevice from enduserdevicemap where fkenduser=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

#Device Mobility that have associate with enduser by userid filter
# select pkid from device where fkenduser_mobility=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

# All Devices that configured with Owner User ID field and have associate with enduser by userid filter
# select name from device where fkenduser=(select eu.pkid from enduser as eu where eu.userid like '<Insert the UserID>')

import urllib3
import zeep
import time
from pprint import pprint
import logging
from lxml import etree
from zeep.exceptions import Fault
from zeep.cache import SqliteCache
from zeep.transports import Transport
import requests
from requests import Session
from sys import exit
import random
from flask import Flask, render_template,request
import werkzeug
from werkzeug.utils import secure_filename
import csv
import io
import os
import datetime
import ldap
import ldap.modlist as modlist
import grp
import math
import time

CUCM_ADDRESS = '10.10.20.1'
CUCM_USERNAME = 'administrator'
CUCM_PASSWORD = 'ciscopsdt'

LDAP_HOST = '10.10.20.100'
LDAP_ADMIN_DN = 'abc\\administrator'
LDAP_PASS = 'ciscopsdt'
LDAP_BASE_DN = 'cn=Users,dc=abc,dc=inc'
HOME_BASE = "/home"

# Create variable for the CUCM 14 AXL WSDL File location
# from the services plugin axl toolkit download page
AXL_WSDL_FILE = "axlsqltoolkit/schema/current/AXLAPI.wsdl"

def show_history():
    for hist in [history.last_sent, history.last_received]:
        print(etree.tostring(hist["envelop"], encoding="unicode", pretty_print=true))

session = requests.Session()
session.verify = False
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
session.auth = requests.auth.HTTPBasicAuth(CUCM_USERNAME, CUCM_PASSWORD)
history = zeep.plugins.HistoryPlugin()
client = zeep.Client(wsdl=AXL_WSDL_FILE, transport=zeep.Transport(session=session), plugins=[history])
service = client.create_service('{http://www.cisco.com/AXLAPIService/}AXLAPIBinding',
                                f'https://{CUCM_ADDRESS}/axl/')
print(service.getCCMVersion())


####################################
#           List all DN
####################################
print("===================== DN ==================")
try:
    resp = service.listLine(searchCriteria={'pattern':'%'},
                            returnedTags={'pattern': '','routePartitionName':'','asciiAlertingName':''})

    #print(resp)  # This prints the raw json dump
    lines = resp # this just saves it into the lines variable

    #now you can loop through the lines and reference the line data
    for line in lines['return']['line']:
        #this print the line pattern, routerpartitionname, and ascii alert
       print(line['pattern'], line['routePartitionName']._value_1,line['asciiAlertingName'])
except Fault:
    show_history()


####################################
#           List Users
####################################
print("===================== USERS ==================")
try:
    resp = service.listUser(searchCriteria={'userid':'%'},returnedTags={'userid':'','telephoneNumber':''})

    print(resp)  # This prints the raw json dump
    users = resp # this just saves it into the lines variable

    #now you can loop through the lines and reference the line data
    #for user in users['return']['line']:
        #this print the line pattern, routerpartitionname, and ascii alert
    #   print(line['pattern'], line['routePartitionName']._value_1,line['asciiAlertingName'])
except Fault:
    show_history()


####################################
#           List Device Profiles
####################################
print("===================== Devices ==================")
try:
    resp = service.listDeviceProfile(searchCriteria={'name':'%'},
                            returnedTags={})

    print(resp)  # This prints the raw json dump
    devices = resp # this just saves it into the lines variable

    #now you can loop through the lines and reference the line data
    #for line in devices['return']['line']:
        #this print the line pattern, routerpartitionname, and ascii alert
       #print(line['pattern'], line['routePartitionName']._value_1,line['asciiAlertingName'])
except Fault:
    show_history()
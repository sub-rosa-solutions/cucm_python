# https://getpractical.co.uk/2021/05/24/cisco-cucm-axl-api-requests-using-python/
# https://developer.cisco.com/docs/axl-schema-reference/
# https://developer.cisco.com/site/axl/

#UNITY Rest calls
# https://www.cisco.com/c/en/us/td/docs/voice_ip_comm/connection/REST-API/CUPI_API/b_CUPI-API/m_cupi-api-user-pwd-pin-setting.html#reference_B32245DBC67A42228AA514C41D708368
# https://collabapilab.ciscolive.com/lab/pod3/Bonus/cupi_api

#start-up sandbox
# https://devnetsandbox.cisco.com/
# search CUCM 14, set time or default is 6 hours which can be extended

# create access group
# 1.  User management --> user settings --> access control group --> new add
# 2. name the group --> axl api access --> save
# 3. top right related links -->  assign role to ACP
# 4. find all --> select standard axl access --> add selected

# create axl user
# 1. User management --> application user --> add new
# 2. user id: axlaccess, password: axlpassword
# 3. add to access control group --> axl api access --> save

# !!!!!!!!!!!!!!! ensure CUCM unity pin sync
# https://www.cisco.com/c/en/us/support/docs/unified-communications/unified-communications-manager-callmanager/200978-How-to-Enable-Common-PIN-for-UCM-and-UC.html

import urllib3
import zeep
from pprint import pprint
import logging
from lxml import etree
from zeep.exceptions import Fault
from zeep.cache import SqliteCache
from zeep.transports import Transport
import requests
from requests import Session
from sys import exit
import random
from flask import Flask, request
import xml.etree.ElementTree as ET

CUCM_ADDRESS = '10.10.20.1'
CUCM_USERNAME = 'administrator'
CUCM_PASSWORD = 'ciscopsdt'

CUC_ADDRESS = '10.10.20.18'
CUC_USERNAME = 'administrator'
CUC_PASSWORD = 'ciscopsdt'

# Create variable for the CUCM 14 AXL WSDL File location
# from the services plugin axl toolkit download page
AXL_WSDL_FILE = "axlsqltoolkit/schema/current/AXLAPI.wsdl"

def show_history():
    for hist in [history.last_sent, history.last_received]:
        print(etree.tostring(hist["envelop"], encoding="unicode", pretty_print=true))

session = requests.Session()
session.verify = False
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
session.auth = requests.auth.HTTPBasicAuth(CUCM_USERNAME, CUCM_PASSWORD)
history = zeep.plugins.HistoryPlugin()
client = zeep.Client(wsdl=AXL_WSDL_FILE, transport=zeep.Transport(session=session), plugins=[history])
service = client.create_service('{http://www.cisco.com/AXLAPIService/}AXLAPIBinding',
                                f'https://{CUCM_ADDRESS}/axl/')
print(service.getCCMVersion())

def find_user(search_type,query,cuc_query):
    print ("Searching for " + search_type + " = " +query)

    if search_type == "userid":
        try:
            resp = service.listUser(searchCriteria={'userid': query},
                                    returnedTags={'userid': '','firstName':'','lastName':'','status': '', 'telephoneNumber':''})
        except Fault:
            show_history()

        #Searching for user

        try:
            user_list = resp['return'].user
            for user in user_list:
                print("User Found")
                info = "userid: <b>" + user['userid'] +"</b><br>"
                print(info)
                info += "firstName: <b>" + user['firstName']+"</b><br>"
                print(info)
                info += "lastName: <b>" + user['lastName']+"</b><br>"
                print(info)
                try:
                    info += "telephoneNumber: <b>" + user['telephoneNumber']+"</b><br>"
                except:
                    info += "telephoneNumber: <br>"
                #print (info)
                return (info,"")
        except:
            return None,None

    else:
        print ("Searching for telephone number...")
        try:
            sql = "SELECT enduser.userid,enduser.firstName,enduser.lastName,enduser.telephoneNumber FROM enduser WHERE telephonenumber = " + query
            resp = service.executeSQLQuery(sql)
        except Fault:
            show_history()

        try:
            info = ""
            for row in resp['return']['row']:
                info += "<u>CUCM info for userid/telephoneNumber:"+query+"</u><br>"
                info += "userid: <b>" + row[0].text + "</b><br>"
                info += "firstName: <b>" + row[1].text + "</b><br>"
                info += "lastName: <b>" + row[2].text + "</b><br>"
                info += "telephoneNumber: <b>" + row[3].text + "</b><br>"
                info += "<font color=red>Confirm <b>"+row[0].text+"</b> PIN reset: <a href=pin_reset?userid=" + row[0].text + "&cuc_userid="+cuc_query+">Yes</a> | <a href=/>No</a></font><br>"
            return info,row[0].text
        except:
            return None,None

def reset_user_pin(userid):
    print ("updating userid:", userid)
    rand_pin = "%05d" % random.randint(0, 99999)
    print ("random pin:",rand_pin)

    try:
        resp = service.updateUser(
                userid=userid,
                pin=rand_pin,
                pinCredentials={'pinCredUserMustChange':'true'}
        )
        print("Successfully updated user pin")
        return (rand_pin)
    except Fault:
        show_history()


def reset_cuc_user_pin(userid,new_pin):
    #html = "updating CUC PIN for userid:" + userid + "<br>"

    # https://www.primates.dev/parsing-an-api-xml-response-data-python/
    #url = f'http://{CUC_ADDRESS}/vmrest/import/users/ldap'
    ##print(url)
    #r = requests.get(url, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
    #print(r, r.content)

    #GET USER OBJECT_ID
    url = f'http://{CUC_ADDRESS}/vmrest/users?query=(alias%20is%20{userid})'
    #print (url)
    r = requests.get(url,verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
    #print(r.content)
    root = ET.fromstring(r.content)
    for child in root.iter('*'):
        #print(child.tag)
        if child.tag == "ObjectId":
            object_id = child.text
    #html += "user object_id:" + object_id +"<br>"

    #UPDATE USER PIN AND UNLOCK
    url = f'https://{CUC_ADDRESS}/vmrest/users/{object_id}/credential/pin'
    data = {
        "Credentials": new_pin,
        "HackCount": 0,
        "TimeHacked": "",
        "CredMustChange":"true"
    }
    #print (url)
    r = requests.put(url, json=data, verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
    print(r.status_code)
    if r.status_code == 204:
        html = "&#x2705; CUC PIN userid: <u>"+userid+"</u>'s PIN has been successfully updated to PIN <font color=blue><b>"+new_pin+"</b></font> and their VM has been unlocked<br><br>"
    else:
        html = "&#10062; Unable to update CUC PIN<br><br>"

    print(html)
    return (html)

app = Flask(__name__)

@app.route('/')
def main_page():
    html = """
    <form name=form method=get action=user_search> 
    <table>
        <tr><td colspan=2 align=center><b>User PIN Reset  </b>
        <tr><td colspan=2  align=right><hr>
        <tr>
            <td>CUCM:
            <td align=right><select name=search_type>
                                <option value=userid>userid
                                <option value=telephone_number>telephone_number
            <td><input type=text name=query><br>
        <tr>
            <TD>Unity:
            <td align=right><input type=text readonly name=cuc_search_type value=userid size=17>
            <td><input type=text name=cuc_query><br>
        <tr><td colspan=2  align=right><hr>
        <tr><td colspan=2 align=center><input type=submit>
    """
    return html

@app.route('/user_search')
def user_search():
    if len(request.args.get("query")) == 0:
        return ("<b><font color=red>Input variable cannot be empty, please enter input text to search</font></b><br><br><a href=/>Back to User Search</a>")
    else:
        query = request.args.get("query")
        search_type = request.args.get("search_type")
        cuc_query = request.args.get("cuc_query")
        cuc_search_type = request.args.get("cuc_search_type")

    html = f"""
    Querying CUCM for <i>{search_type}</i> for <b>{query}</b> to see if it exists, Querying CUC for <i>userid:</i><b>"{cuc_query}"</b><br>
    """
    user_found,found_userid = find_user(search_type,query,cuc_query)

    cuc_html = "<br><u>Unity info for userid: "+cuc_query + "</u><Br>"
    #GET USER OBJECT_ID
    url = f'http://{CUC_ADDRESS}/vmrest/users?query=(alias%20is%20{cuc_query})'
    #print (url)
    r = requests.get(url,verify=False, auth=(CUC_USERNAME, CUC_PASSWORD))
    #print(r.content)
    root = ET.fromstring(r.content)
    for child in root.iter('*'):
        #print(child.tag,child.text)

        if child.tag == "FirstName":
            cuc_html += "firstName: <b>"+child.text+"</b><br>"
        if child.tag == "LastName":
            cuc_html += "lastName: <b>"+child.text+"</b><br>"
        if child.tag == "Alias":
            cuc_html += "userid: <b>"+child.text+"</b><br><br>"

    #print (user_found)

    if user_found == None:
        html += "<br><b><font color=red>User not found, please try again</font></b><br> <br><a href=/>Back to User Search</a>"
        return html
    else:

        if search_type == "userid":
            userid = query
            #html += user_found
            html += "<br><u>CUCM info for userid:"+query+"</u><br>" + user_found
            html += cuc_html
            html += "Please confirm user PIN reset <a href=pin_reset?userid=" + userid + "&cuc_userid="+cuc_query+">Yes</a> | <a href=/>No</a>"
            html += "<br><br><a href=/>Back to main menu</a>"
            return html
        else:
            userid = found_userid
            html +=  "<br>" + user_found
            html +=  cuc_html
            html += "<a href=/>Back to main menu</a>"
            return html


@app.route('/pin_reset')
def pin_reset_confirmed():

    userid = request.args.get("userid")
    new_pin = reset_user_pin(userid)

    cuc_userid = request.args.get("cuc_userid")
    cuc_sync = reset_cuc_user_pin(cuc_userid,new_pin)

    if new_pin is not None:
        html = "<h2>Updating CUCM and CUC User PIN</h2>"
        html += "&#x2705; CUCM userid: <u>"+userid+"</u>'s PIN has been successfully updated to <font color=blue><b>"+new_pin+"</b></font><BR>"
        html += cuc_sync
        html += "<a href=/>Back to User PIN Reset Search</a>"
        return html
    else:
        return "Something went wrong, please contact support"

if __name__ == '__main__':
    app.run(debug=True, port=5001)